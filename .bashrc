#! /bin/bash

#+ Pathing
#- Source '"${HOME}"/.shrc.d/shrc.sh' if it exists
if [ -e "${HOME}"/.shrc.d/ ] ; then
  . "${HOME}"/.shrc.d/shrc.sh >/dev/null 2>&1
  . "${HOME}"/.shrc.d/path.sh >/dev/null 2>&1
  while IFS= read -r -d '' f ; do
    . "${f}" >/dev/null 2>&1
  done < <(find "${HOME}"/.shrc.d/* '!' -name 'shrc.sh' '!' -name 'path.sh' -print0)
fi

#+ Interactive mode check
#- expand aliases if in non interactive mode
shopt -s expand_aliases

#- If not running interactively, don't do anything
[[ $- != *i* ]] && return

#+ Tab completion
# [ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#- Tab completion with sudo
complete -cf sudo

#+ Window options
#- Make connections to the X server
xhost +local:root > /dev/null 2>&1

#- Change the window title of X terminals
case ${TERM} in
  xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
    ;;
  screen*)
    PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
    ;;
esac

#- Check window size
# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

#+ History
#- Erase duplicates in history
export HISTCONTROL=erasedups

#- Enable history appending instead of overwriting.  #139609
shopt -s histappend

#+ Aliases
alias l='ls -lah --color=auto'
alias ll='ls -lh --color=auto'
alias la='ls -A --color=auto'

#+ Usability
# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

#+ Colors and basic prompt
colors() {
  local fgc bgc vals seq0

  printf "Color escapes are %s\n" '\e[${value};...;${value}m'
  printf "Values 30..37 are \e[33mforeground colors\e[m\n"
  printf "Values 40..47 are \e[43mbackground colors\e[m\n"
  printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

  # foreground colors
  for fgc in {30..37}; do
    # background colors
    for bgc in {40..47}; do
      fgc=${fgc#37} # white
      bgc=${bgc#40} # black

      vals="${fgc:+$fgc;}${bgc}"
      vals=${vals%%;}

      seq0="${vals:+\e[${vals}m}"
      printf "  %-9s" "${seq0:-(default)}"
      printf " ${seq0}TEXT\e[m"
      printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
    done
    echo; echo
  done
}

use_color=true

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
  && type -P dircolors >/dev/null \
  && match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

if ${use_color} ; then
  # Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
  if type -P dircolors >/dev/null ; then
    if [[ -f ~/.dir_colors ]] ; then
      eval $(dircolors -b ~/.dir_colors)
    elif [[ -f /etc/DIR_COLORS ]] ; then
      eval $(dircolors -b /etc/DIR_COLORS)
    fi
  fi

  if [[ ${EUID} == 0 ]] ; then
    PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
  else
    if [ -e /etc/profile.d/git-prompt.sh ] ; then
      shopt -q login_shell || . /etc/profile.d/git-prompt.sh
    else
      PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '
    fi
  fi

  alias ls='ls --color=auto'
  alias grep='grep --colour=auto'
  alias egrep='egrep --colour=auto'
  alias fgrep='fgrep --colour=auto'
else
  if [[ ${EUID} == 0 ]] ; then
    # show root@ when we don't have colors
    PS1='\u@\h \W \$ '
  else
    PS1='\u@\h \w \$ '
  fi
fi

unset use_color safe_term match_lhs sh

#+ Prompt
# Check if starship (https://starship.rs/) is not installed, ask to install. else, use
pkg=starship
if which "${pkg}" > /dev/null 2>&1 ; then
  if [ ! -e /usr/local/bin/starship ] ; then
    install_starship
    printf "\e[0;32m:: Success - opening a new bash terminal to use starship.\e[0m\n"
    exec bash
  else
    #- init
    eval "$(starship init bash)"
  fi
fi

#+ Kubernetes
#- Check for kubectl
pkg=kubectl
if which "${pkg}" > /dev/null 2>&1 ; then
  #- Use kubectl autocomplete
  source <(kubectl completion bash)
  alias k=kubectl
  complete -F __start_kubectl k
  #- Install Krew if not installed
  indep=krew
  if ! "${pkg}"-"${indep}" version > /dev/null 2>&1 ; then
    (
      set -x; cd "$(mktemp -d)" &&
      curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
      tar zxvf krew.tar.gz &&
      KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
      "$KREW" install --manifest=krew.yaml --archive=krew.tar.gz &&
      "$KREW" update
    )
    # export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
    # NOTE: pathing is automatic when using https://gitlab.com/toms-dotfiles/sh
    exec bash
    # TODO: auto install krew packages if they are not installed
  fi
fi

#+ Terraform
pkg=terraform
if which "${pkg}" > /dev/null 2>&1 ; then
  complete -C /usr/bin/terraform terraform
fi

#+ GCP
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/usr/local/bin/google-cloud-sdk/path.bash.inc' ]; then . '/usr/local/bin/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/usr/local/bin/google-cloud-sdk/completion.bash.inc' ]; then . '/usr/local/bin/google-cloud-sdk/completion.bash.inc'; fi
