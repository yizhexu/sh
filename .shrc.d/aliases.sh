#- list directory contents
alias ls='ls -G'
alias lsa='ls -lah'
alias la='ls -lAh'

# color output
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# editor
export VISUAL='emacs'
export EDITOR='nano'
alias vi='vim'

# Misc. Aliases
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias np='nano -w PKGBUILD'
alias more='less -R'
alias tree='tree -C'
alias less='less -R'                      # display colors correctly

# internet
case "$(uname -s)" in
  "Linux" )
    #alias local_ip='ifconfig getifaddr en0'
    ;;
  "Darwin" )
    alias local_ip='ipconfig getifaddr en0'
    alias whois='whois -h whois-servers.net'
    ;;
esac
alias ext_ip='curl ipinfo.io/ip'
alias ips="ifconfig -a | grep -o 'inet6\? \(addr:\)\?\s\?\(\(\([0-9]\+\.\)\{3\}[0-9]\+\)\|[a-fA-F0-9:]\+\)' | awk '{ sub(/inet6? (addr:)? ?/, \"\"); print }'"
alias speedtest='echo "scale=2; `curl  --progress-bar -w "%{speed_download}" http://speedtest.wdc01.softlayer.com/downloads/test10.zip -o /dev/null` / 131072" | bc | xargs -I {} echo {} mbps'
alias p8='ping 8.8.8.8'
alias pg='ping google.com'

# package managers
case "$(uname -s)" in
  "Linux" )
    yay_upgrade() {
      yay --clean --noconfirm && yay -S -cc --noconfirm && yay -S -yy --noconfirm && yay -S -uu --combinedupgrade --redownloadall --rebuildtree --noconfirm --nodiffmenu --removemake --cleanafter
    }
    yay_install() {
      yay -S "${1}" --bottomup --redownloadall --rebuildtree --noconfirm --nodiffmenu --removemake --cleanafter ; yay --clean --noconfirm
    }
    ;;
  "Darwin" )
    alias brew_upgrade='brew update && brew upgrade; brew cleanup'
    brew_install() {
      brew install "${1}" || brew cask install "${1}"
    }
    ;;
esac

# MISC.
case "$(uname -s)" in
  "Linux" )
    true
    ;;
  "Darwin" )
    # Recursively delete `.DS_Store` files
    alias mac_cleanup="find . -type f -name '*.DS_Store' -ls -delete"
    # Clear download history from quarantine. https://mths.be/bum
    alias mac_emptytrash="sudo rm -rfv /Volumes/*/.Trashes; sudo rm -rfv ~/.Trash; sudo rm -rfv /private/var/log/asl/*.asl"
    # Show/hide hidden files in Finder
    alias finder_show="defaults write com.apple.finder AppleShowAllFiles -bool true && killall Finder"
    alias finder_hide="defaults write com.apple.finder AppleShowAllFiles -bool false && killall Finder"
    # Hide/show all desktop icons (useful when presenting)
    alias mac_hidedesktop="defaults write com.apple.finder CreateDesktop -bool false && killall Finder"
    alias mac_showdesktop="defaults write com.apple.finder CreateDesktop -bool true && killall Finder"
    # Merge PDF files
    # Usage: `mergepdf -o output.pdf input{1,2,3}.pdf`
    alias mergepdf='/System/Library/Automator/Combine\ PDF\ Pages.action/Contents/Resources/join.py'
    # Lock the screen (when going AFK)
    alias afk="/System/Library/CoreServices/Menu\ Extras/User.menu/Contents/Resources/CGSession -suspend"
    # wifi password
    alias wifipass="security find-generic-password -g -D \"AirPort network password\" -w -a"
    alias wifipow="/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -s"
    # use gnu date from coreutils
    # alias date='gdate' # may already be set in ``.shrc.d/path.sh`
    # TODO: need to alias manpages for all gnu apps instead of dumb mac ones
    usage() {
      du -sch "${@}" | sort -h
    }
    ;;
esac

#+ ex (archive extractor)
#-EXAMPLE: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

#+ ssh
#- default active keep alive
alias ssh='ssh -o TCPKeepAlive=yes -o ServerAliveInterval=120'
#- ssh from jump/bastion server to inteneded host on intended port as specified user
# EG: `ssh_jump ${USER} jump001.com workstation001.com 22`
ssh_jump () {
  ssh -o TCPKeepAlive=yes -o ServerAliveInterval=120 -o ProxyCommand="ssh -o TCPKeepAlive=yes -o ServerAliveInterval=120 ${2} nc ${3} ${4}" "${1}"@"${3}"
}

#+ fix baddly maintained firewall issues
#- wget
if [ ! $(wget -q --spider https://github.com) ] ; then
  alias wget='wget --no-check-certificate -L'
fi
#- curl
if [ ! $(curl https://github.com > /dev/null 2>&1) ] ; then
  alias curl='curl -k'
fi
#- Make Azure cli usable when behind proxy
# TODO: test for SSL errors first
export AZURE_CLI_DISABLE_CONNECTION_VERIFICATION=anycontent